# vueformat

## 先安装项目依赖 npm i

## 安装依赖之后可以 npm run serve 启动项目

## 项目带有自动格式化，在 vsCode 里下载插件

    1.ESLint
    2.Prettier - Code formatter

    .eslintrc.js	配置检查项
    .prettierrc		配置代码风格

## menu 说明

menu 样例

```js
<el-menu router background-color="#334054" text-color="white" class="menu">
  <el-menu-item index="/test">
    <template slot="title">
      <i class="el-icon-location"></i>
      <span>首页</span>
    </template>
  </el-menu-item>
  <el-submenu index="sdsd">
    <template slot="title">
      <i class="el-icon-location"></i>
      <span>用户管理</span>
    </template>
    <el-menu-item index="sdsd">
      <template slot="title">
        <i class="el-icon-location"></i>
        <span>查询用户</span>
      </template>
    </el-menu-item>
  </el-submenu>
</el-menu>;

// 数据
menus = [
  { title: "首页", url: "/test", icon: "el-icon-location", level: 1 },
  {
    title: "用户管理",
    url: "/ddd",
    icon: "el-icon-location",
    level: 1,
    children: [
      {
        title: "查询用户",
        url: "/ddd",
        icon: "el-icon-location",
        level: 2
      }
    ]
  }
];
```

## 页面 loading

```js
// 打开loading
const loading = this.$loading({
  lock: true,
  text: "Loading",
  spinner: "el-icon-loading",
  background: "rgba(0, 0, 0, 0.7)"
});
// 关闭loading
loading.close();
```
