// 这里主要配置开发环境的端口号，避免重复
// ref	https://cli.vuejs.org/zh/config/#vue-config-js

// 获取当前的网络地址
const os = require("os");
var ifaces = os.networkInterfaces();
let networkAddress = ((ifaces["en0"] || []).filter(item => item.family === "IPv4")[0] || {}).address || "noAddress";
const PORT = 2233;
const SERVE_PATH = `"http://${networkAddress}:${PORT}"`;

module.exports = {
  devServer: {
    open: true,
    port: 8090,
    https: false,
    hotOnly: false
  },
  // 自定义Vue打包插件的配置
  // 这里主要目的是 把 SERVE_PATH 写入到 process.env（可以修改 process.env）
  chainWebpack: config => {
    // args 是一个数组，返回值也必须是一个数组
    config.plugin("define").tap(args => {
      //   console.log("define", args);
      let newProcessEnv = { ...args[0]["process.env"], SERVE_PATH };
      return [{ "process.env": newProcessEnv }];
    });
    // 修改项目的的标题
    config.plugin("html").tap(args => {
      // args[0].title = "TODO: change title";
      args[0].title = "XX管理系统";
      return args;
    });
  }
};
