import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";

Vue.use(VueRouter);

const routes = [
  { path: "*", redirect: "/notFind" },
  {
    path: "/",
    name: "Home",
    component: Home,
    redirect: "/home",
    children: [
      { path: "/home", name: "Index", component: () => import("../views/index.vue") },
      // 系统管理
      { path: "/dataReport", name: "DataReport", component: () => import("../views/setting/dataReport.vue") },
      { path: "/iconManage", name: "IconManage", component: () => import("../views/setting/iconManage.vue") },
      // 权限管理
      { path: "/api", name: "Api", component: () => import("../views/auth/api.vue") },
      { path: "/menu", name: "Menu", component: () => import("../views/auth/menu.vue") },
      { path: "/user", name: "User", component: () => import("../views/auth/user.vue") }
      // { path: "/noPermission", name: "NoPermission", component: () => import("../views/404.vue") }
    ]
  },
  { path: "/login", name: "Login", component: () => import("../views/Login.vue") },
  { path: "/notFind", name: "notFind", component: () => import("../views/404.vue") }
  // 备用的404页面
  // { path: "/notFind", name: "notFind", component: () => import("../views/404_copy.vue") }
];

const router = new VueRouter({
  routes
});

//挂载路由导航守卫,拦截没有权限
router.beforeEach((to, from, next) => {
  //to 将要访问的路径
  //from 代表从哪个路径跳转过来
  //next 是一个函数 表示放行
  // 注意点：没有存放在权限里面的公共页面要放开
  if (["/login", "/notFind"].includes(to.path)) return next();

  let isLogin = JSON.parse(localStorage.getItem("isLogin"));
  let nowAuth = JSON.parse(localStorage.getItem("menuList"));

  console.log("isLogin", isLogin);
  if (isLogin !== true) return next("/login");

  let hasAuth = false;
  // 判断当前用户是否拥有权限
  for (let i = 0; i < (nowAuth || []).length; i++) {
    let item = nowAuth[i];
    if (item.url === to.path && !hasAuth) {
      hasAuth = true;
      break;
    } else {
      if (item.children && item.children.length > 0 && !hasAuth) {
        for (let j = 0; j < item.children.length; j++) {
          let child = item.children[j];
          if (child.url === to.path) {
            hasAuth = true;
            break;
          }
        }
      }
    }
  }
  if (!hasAuth) {
    return next("/notFind");
  }

  next();
});

export default router;
