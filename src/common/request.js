// 封装接口
import axios from "axios";
import qs from "qs";

// 联调地址
let { SERVE_PATH: baseURL, NODE_ENV } = process.env;

// 本地接口地址
baseURL = "http://localhost:2233";
// 线上接口
// baseURL = "http://175.24.174.41:2233";

// 线上环境
if (NODE_ENV === "production") {
  baseURL = "http://175.24.174.41:2233";
}

console.log("BASEURL", baseURL);
console.log("NODE_ENV", NODE_ENV);

const ajax = axios.create({
  baseURL,
  timeout: 10000,
  headers: {
    "Content-Type": "application/json;charset=utf-8"
  }
});
export default {
  get: async (url, params, config = {}) => {
    try {
      let res = await ajax.get(url, { params, ...config });
      return res.data;
    } catch (error) {
      throw new Error(error);
    }
  },
  post: async (url, params, config = {}) => {
    try {
      let res = await ajax.post(url, params, config);
      return res.data;
    } catch (error) {
      throw new Error(error);
    }
  },
  postForm: async (url, params, config = {}) => {
    try {
      let res = await ajax.post(url, qs.stringify(params), {
        ...config,
        headers: {
          // 请求表单数据的请求头
          "Content-Type": "application/x-www-form-urlencoded;charset=utf-8"
        }
      });
      return res.data;
    } catch (error) {
      throw new Error(error);
    }
  }
};
