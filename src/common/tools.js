import jsonp from "jsonp";

// 格式化日期
export function formateDate(time) {
  if (!time) return "";
  let date = new Date(time);
  return (
    date.getFullYear() +
    "-" +
    (date.getMonth() + 1) +
    "-" +
    date.getDate() +
    " " +
    date.getHours() +
    ":" +
    date.getMinutes() +
    ":" +
    date.getSeconds()
  );
}

// 获取天气
/**
 *
 * @param {*} province 省份
 * @param {*} city 城市
 * @returns
 */
export const reqWeather = (province, city) => {
  return new Promise((resolve, reject) => {
    const url = `https://wis.qq.com/weather/common?source=pc&weather_type=forecast_24h&province=${province}&city=${city}&_=1617346677888`;
    jsonp(url, {}, (err, data) => {
      if (!err && data.message === "OK") {
        resolve(data.data.forecast_24h[1].day_weather_short);
      } else {
        reject(err);
      }
    });
  });
};

// 每日一句
export const dailySentence = () => {
  return new Promise((resolve, reject) => {
    jsonp("https://v1.hitokoto.cn/?c=a&c=b&c=c&c=d", {}, (err, data) => {
      if (!err) {
        resolve(JSON.parse(data));
      } else {
        reject(err);
      }
    });
  });
};
