// 一些写死的数据
export let iconList = [
  {
    dictId: 8,
    dictName: "饿了吗",
    dictValue: "",
    dictCode: "el-icon-platform-eleme",
    parentId: 4,
    gmtCreate: "2021-12-01 09:51:14",
    hasChildren: false
  },
  {
    dictId: 9,
    dictName: "饿了吗",
    dictValue: null,
    dictCode: "el-icon-eleme",
    parentId: 4,
    gmtCreate: "2021-12-01 09:51:14",
    hasChildren: false
  },
  {
    dictId: 10,
    dictName: "删除",
    dictValue: null,
    dictCode: "el-icon-delete-solid",
    parentId: 4,
    gmtCreate: "2021-12-01 09:51:14",
    hasChildren: false
  },
  {
    dictId: 11,
    dictName: "删除",
    dictValue: null,
    dictCode: "el-icon-delete",
    parentId: 4,
    gmtCreate: "2021-12-01 09:51:14",
    hasChildren: false
  },
  {
    dictId: 12,
    dictName: "设置",
    dictValue: null,
    dictCode: "el-icon-s-tools",
    parentId: 4,
    gmtCreate: "2021-12-01 09:51:14",
    hasChildren: false
  },
  {
    dictId: 13,
    dictName: "设置",
    dictValue: null,
    dictCode: "el-icon-setting",
    parentId: 4,
    gmtCreate: "2021-12-01 09:51:14",
    hasChildren: false
  },
  {
    dictId: 14,
    dictName: "用户",
    dictValue: null,
    dictCode: "el-icon-user-solid",
    parentId: 4,
    gmtCreate: "2021-12-01 09:51:14",
    hasChildren: false
  },
  {
    dictId: 15,
    dictName: "用户",
    dictValue: null,
    dictCode: "el-icon-user",
    parentId: 4,
    gmtCreate: "2021-12-01 09:51:14",
    hasChildren: false
  },
  {
    dictId: 16,
    dictName: "电话",
    dictValue: null,
    dictCode: "el-icon-phone",
    parentId: 4,
    gmtCreate: "2021-12-01 09:51:14",
    hasChildren: false
  },
  {
    dictId: 17,
    dictName: "电话",
    dictValue: null,
    dictCode: "el-icon-phone-outline",
    parentId: 4,
    gmtCreate: "2021-12-01 09:51:14",
    hasChildren: false
  },
  {
    dictId: 18,
    dictName: "更多",
    dictValue: null,
    dictCode: "el-icon-more",
    parentId: 4,
    gmtCreate: "2021-12-01 09:51:14",
    hasChildren: false
  },
  {
    dictId: 19,
    dictName: "星星",
    dictValue: null,
    dictCode: "el-icon-star-on",
    parentId: 4,
    gmtCreate: "2021-12-01 09:51:14",
    hasChildren: false
  },
  {
    dictId: 20,
    dictName: "星星",
    dictValue: null,
    dictCode: "el-icon-star-off",
    parentId: 4,
    gmtCreate: "2021-12-01 09:51:14",
    hasChildren: false
  },
  {
    dictId: 21,
    dictName: "购物",
    dictValue: null,
    dictCode: "el-icon-s-goods",
    parentId: 4,
    gmtCreate: "2021-12-01 09:51:14",
    hasChildren: false
  },
  {
    dictId: 22,
    dictName: "购物",
    dictValue: null,
    dictCode: "el-icon-goods",
    parentId: 4,
    gmtCreate: "2021-12-01 09:51:14",
    hasChildren: false
  },
  {
    dictId: 23,
    dictName: "警告",
    dictValue: null,
    dictCode: "el-icon-warning",
    parentId: 4,
    gmtCreate: "2021-12-01 09:51:14",
    hasChildren: false
  },
  {
    dictId: 24,
    dictName: "感叹号",
    dictValue: null,
    dictCode: "el-icon-warning-outline",
    parentId: 4,
    gmtCreate: "2021-12-01 09:51:14",
    hasChildren: false
  },
  {
    dictId: 25,
    dictName: "问号",
    dictValue: null,
    dictCode: "el-icon-question\n",
    parentId: 4,
    gmtCreate: "2021-12-01 09:51:14",
    hasChildren: false
  },
  {
    dictId: 26,
    dictName: "信息",
    dictValue: null,
    dictCode: "el-icon-info",
    parentId: 4,
    gmtCreate: "2021-12-01 09:51:14",
    hasChildren: false
  },
  {
    dictId: 27,
    dictName: "缩小",
    dictValue: null,
    dictCode: "el-icon-remove\n",
    parentId: 4,
    gmtCreate: "2021-12-01 09:51:14",
    hasChildren: false
  },
  {
    dictId: 28,
    dictName: "放大",
    dictValue: null,
    dictCode: "el-icon-circle-plus",
    parentId: 4,
    gmtCreate: "2021-12-01 09:51:14",
    hasChildren: false
  },
  {
    dictId: 29,
    dictName: "正确",
    dictValue: null,
    dictCode: "el-icon-success",
    parentId: 4,
    gmtCreate: "2021-12-01 09:51:14",
    hasChildren: false
  },
  {
    dictId: 30,
    dictName: "错误",
    dictValue: null,
    dictCode: "el-icon-error",
    parentId: 4,
    gmtCreate: "2021-12-01 09:51:14",
    hasChildren: false
  },
  {
    dictId: 31,
    dictName: "放大",
    dictValue: null,
    dictCode: "el-icon-zoom-in",
    parentId: 4,
    gmtCreate: "2021-12-01 09:51:14",
    hasChildren: false
  },
  {
    dictId: 32,
    dictName: "缩小",
    dictValue: null,
    dictCode: "el-icon-zoom-out",
    parentId: 4,
    gmtCreate: "2021-12-01 09:51:14",
    hasChildren: false
  },
  {
    dictId: 33,
    dictName: "删除",
    dictValue: null,
    dictCode: "el-icon-remove-outline",
    parentId: 4,
    gmtCreate: "2021-12-01 09:51:14",
    hasChildren: false
  },
  {
    dictId: 34,
    dictName: "新增",
    dictValue: null,
    dictCode: "el-icon-circle-plus-outline",
    parentId: 4,
    gmtCreate: "2021-12-01 09:51:14",
    hasChildren: false
  },
  {
    dictId: 35,
    dictName: "查看",
    dictValue: null,
    dictCode: "el-icon-circle-check\n",
    parentId: 4,
    gmtCreate: "2021-12-01 09:51:14",
    hasChildren: false
  },
  {
    dictId: 36,
    dictName: "关闭",
    dictValue: null,
    dictCode: "el-icon-circle-close",
    parentId: 4,
    gmtCreate: "2021-12-01 09:51:14",
    hasChildren: false
  },
  {
    dictId: 37,
    dictName: "帮助",
    dictValue: null,
    dictCode: "el-icon-s-help",
    parentId: 4,
    gmtCreate: "2021-12-01 09:51:14",
    hasChildren: false
  },
  {
    dictId: 38,
    dictName: "帮助",
    dictValue: null,
    dictCode: "el-icon-help",
    parentId: 4,
    gmtCreate: "2021-12-01 09:51:14",
    hasChildren: false
  },
  {
    dictId: 39,
    dictName: "减号",
    dictValue: null,
    dictCode: "el-icon-minus",
    parentId: 4,
    gmtCreate: "2021-12-01 09:51:14",
    hasChildren: false
  },
  {
    dictId: 40,
    dictName: "加号",
    dictValue: null,
    dictCode: "el-icon-plus",
    parentId: 4,
    gmtCreate: "2021-12-01 09:51:14",
    hasChildren: false
  },
  {
    dictId: 41,
    dictName: "对号",
    dictValue: null,
    dictCode: "el-icon-check",
    parentId: 4,
    gmtCreate: "2021-12-01 09:51:14",
    hasChildren: false
  },
  {
    dictId: 336,
    dictName: "el-icon-shopping-bag-1",
    dictValue: null,
    dictCode: "el-icon-shopping-bag-1",
    parentId: 4,
    gmtCreate: "2021-12-01 09:51:14",
    hasChildren: false
  },
  {
    dictId: 337,
    dictName: "el-icon-shopping-bag-2",
    dictValue: null,
    dictCode: "el-icon-shopping-bag-2",
    parentId: 4,
    gmtCreate: "2021-12-01 09:51:14",
    hasChildren: false
  }
];

export let menuList = [
  { id: 1, title: "首页", url: "/home", icon: "el-icon-s-home", level: 1 },
  {
    id: 2,
    title: "权限管理",
    url: "/menu",
    icon: "el-icon-s-tools",
    level: 1,
    children: [
      {
        id: 21,
        title: "菜单管理",
        url: "/menu",
        icon: "el-icon-menu",
        level: 2
      },
      {
        id: 22,
        title: "用户管理",
        url: "/role",
        icon: "el-icon-user-solid",
        level: 2
      },
      {
        id: 23,
        title: "接口管理",
        url: "/api",
        icon: "el-icon-s-order",
        level: 2
      }
    ]
  },
  {
    id: 3,
    title: "系统管理",
    url: "/iconManage",
    icon: "el-icon-s-tools",
    level: 1,
    children: [
      {
        id: 31,
        title: "图标管理",
        url: "/iconManage",
        icon: "el-icon-s-grid",
        level: 2
      },
      {
        id: 32,
        title: "数据战报",
        url: "/dataReport",
        icon: "el-icon-s-data",
        level: 2
      }
    ]
  }
];
