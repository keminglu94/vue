import request from "@/common/request";

// 获取权限
export const getAuth = params => request.postForm("/auth/getAuth", params);

// 修改权限
export const updateAuth = params => request.postForm("/auth/updateAuth", params);

// 增加权限
export const addAuth = params => request.postForm("/auth/addAuth", params);
