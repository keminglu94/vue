import request from "@/common/request";

// get 请求例子
export const getUserList = () => request.get("/user/list");

// post 请求例子
export const delUser = params => request.postForm("user/del", params);
