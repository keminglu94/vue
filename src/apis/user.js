import request from "@/common/request";

// 登录
export const login = params => request.postForm("/user/login", params);

// 注册
export const addUser = params => request.postForm("/user/add", params);

// 用户列表
export const getUserList = () => request.get("/user/list");

// 修改用户信息
export const updateUser = params => request.postForm("/user/update", params);

// 删除用户
export const delUser = params => request.postForm("user/del", params);
